import os
import shutil
from pathlib import Path
from typing import List, Union

from omegaconf import DictConfig
from skimage.metrics import structural_similarity as compare_ssim
import cv2


class Met3dException(Exception):
    pass


def create_pipeline_configuration_file(folder: Union[str, Path]):
    pipeline_template_path = os.path.join(os.path.dirname(__file__), "templates", "template.pipeline.cfg")
    with open(pipeline_template_path, "r") as f:
        template = f.read()
    template = template.replace("%PATH_TO_FOLDER%", os.path.realpath(folder))
    with open("temp.pipeline.cfg", "w") as f:
        f.write(template)


def create_frontend_configuration_file(session_file="compression_01"):
    frontend_template_path = os.path.join(os.path.dirname(__file__), "templates", "template.frontend.cfg")
    with open(frontend_template_path, "r") as f:
        template = f.read()
    template = template.replace("%KIND%", str(session_file))
    with open("temp.frontend.cfg", "w") as f:
        f.write(template)


# Function to launch a shell script from python
def launch_shell_script(script_path: str):
    import subprocess
    subprocess.call(script_path, shell=True, executable='/bin/bash')


# Compute the structural similarity index between two images from the file paths
def structural_similarity(img1_path: Path, img2_path: Path):

    assert img1_path.exists()
    assert img2_path.exists()

    img1 = cv2.imread(str(img1_path))
    img2 = cv2.imread(str(img2_path))
    gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

    return compare_ssim(gray1, gray2, full=False)


# Define path to the bash script
path_to_bash_script = "utils4met3d/batchmode.sh"


def get_configuration():
    import traceback
    stack = traceback.extract_stack()
    caller = stack[-2].filename
    # To avoid reinventing the wheel I'll just use omegaconf as it seems like a reliable library.
    from omegaconf import OmegaConf
    from pathlib import Path
    script_folder = Path(caller).parent
    # The configuration file that is in the same folder than the script will be used.
    configuration_file = script_folder / "configuration.yaml"
    return OmegaConf.load(configuration_file)


def save_figure(screenshot_path: Path, destination: Path):
    ####################################################################################################################
    # Check if the screen shot exits and if it does, move it to the screenshot folder
    if os.path.exists(screenshot_path):
        # Move it
        shutil.move(screenshot_path, destination)
        print(f"The figure has been created and saved as {destination!r}")
        return True
    else:
        raise Met3dException(
            f"Screenshot file does not exist. It should be {screenshot_path!r}")
    ####################################################################################################################


def run_case(data_folder: Union[str, Path], session: str, session_folder: str, screenshot_path: Path, destination: Path,
             manual_run: bool=False):
    """
    Run a case using met3d. The kind of plot will be determined by the session selected.
    The session folder and the screenshot path are parameters that are specified in the configuration file.
    The destination path is the path where the figure will be saved.

    :param data_folder: Path to the folder containing the data
    :param session: name of the session to run
    :param session_folder: Local met3d session folder
    :param screenshot_path: Path to where the screenshot is saved from met3d
    :param destination: Path where the figure will be saved
    :return:
    """

    # Create frontend file from the templates and place session file in proper folder
    # The frontend configuration file is common between the reference and the compressed case
    create_frontend_configuration_file(session_file=session)
    # Place session file in the user met3d folder

    session_file = Path(__file__).parent / "sessions" / f"{session}.session.config"
    # Copy sessions file to sessions folder
    shutil.copyfile(session_file, os.path.join(session_folder, os.path.basename(session_file)))

    create_pipeline_configuration_file(data_folder)

    if manual_run:
        print(f"File name should be '{screenshot_path.name}'")
        input("Manually create the plot and press to continue...")
    else:
        # Launch met3D in batch mode.
        launch_shell_script(path_to_bash_script)
    save_figure(screenshot_path, destination)


def setup_functions(configuration: DictConfig):
    """
    To run a case, we need to provide the data folder, the kind of session, where the session files for met3d, where
    the final screenshot is created and where to save the figure. To avoid repeating all these arguments all the time,
    we use partial to create a function that will take only the data folder and the destination path for the figure.
    :param configuration:
    :return: Function to run a case only providing the data folder and the destination path for the figure.
    """
    ####################################################################################################################
    # Get some parameters from the configuration file
    # Path where the screenshot is created
    screenshot_path = Path(configuration.screenshot).expanduser()
    # Kind of plot that will be created
    session = configuration.kind

    # manual_run = session == "compression_02"
    manual_run = False

    # Directory where the met3d sessions are stored
    session_folder = Path(configuration.met3d.sessions).expanduser()
    ####################################################################################################################

    # Use partial to get a function to run the session as it is specified in the configuration file
    from functools import partial
    run_session = partial(run_case,
                          session=session,
                          session_folder=session_folder,
                          screenshot_path=screenshot_path,
                          manual_run=manual_run)
    return run_session


def get_metrics(files: Union[List[str], List[Path]], compression: Union[dict, str, None]):
    import enstools.io
    from enstools.compression.emulation import emulate_compression_on_dataset

    results = []
    for file in files:
        with enstools.io.read(file) as dataset:
            _, metrics = emulate_compression_on_dataset(dataset, compression)
            results.append(metrics)
    return {**results[0], **results[1]}


def get_compression_ratio(files: Union[List[str], List[Path]], compression: Union[dict, str, None], session="compression_01"):
    metrics = get_metrics(files, compression)
    # Depending on the compression, the actual way of compute the compression ratio is different because
    # the involved variables are different
    if session == "compression_01":
        bit_rates = [32./metrics[var]["compression_ratio"] for var in ["u", "v", "t"]]
        print(f"{bit_rates=}")
        bit_rate = sum(bit_rates)
        print(f"{bit_rate=}")
        compression_ratio = (32 * 3) / bit_rate
        return compression_ratio
