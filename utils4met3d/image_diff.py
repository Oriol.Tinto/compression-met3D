"""
Code to find differences between two images using OpenCV.

This code is largely based on the following tutorial:

https://pyimagesearch.com/2017/06/19/image-difference-with-opencv-and-python/

"""

# import the necessary packages
from skimage.metrics import structural_similarity as compare_ssim
import numpy as np
import imutils
import cv2


def create_merged_figure_with_boxes(path_to_A: str, path_to_B: str, output: str = "merged.png", number_of_boxes: int = 5,
                                    save_all_figures: bool = False) -> None:
    """
    Creates a merged figure with boxes to make identification of differences easier.
    Returns a figure with the merged images with highlight boxes.

    :param path_to_A: path to image A
    :param path_to_B: path to image B
    :param output: path to output image
    :param number_of_boxes: number of boxes to be created
    :param save_all_figures: if True, individual figures will be saved including the differences and the thresholds
    :return:
    """
    # Load the images
    imageA = cv2.imread(path_to_A)
    imageB = cv2.imread(path_to_B)

    # Convert the images to grayscale
    grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
    grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)

    # Compute the Structural Similarity Index (SSIM) between the two images,
    # ensuring that the difference image is returned
    (score, diff) = compare_ssim(grayA, grayB, full=True)
    diff = (diff * 255).astype("uint8")
    print("SSIM: {}".format(score))

    # Threshold the difference image,
    # followed by finding contours to obtain the regions of the two input images that differ
    thresh = cv2.threshold(diff, 0, 255,
                           cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_NONE)

    cnts = imutils.grab_contours(cnts)

    # loop over the contours
    differences = {}
    positions = {}

    for index, c in enumerate(cnts):
        # Compute the bounding box of the contour and then draw the bounding box
        # on both input images to represent where the two images differ.
        (x, y, w, h) = cv2.boundingRect(c)
        positions[index] = (x, y, w, h)

        sl = [slice(y, y + h + 1), slice(x, x + w + 1)]

        case_diff = np.array(diff[tuple(sl)])
        case_diff = case_diff - np.ones(shape=case_diff.shape) * 255
        differences[index] = np.sum(np.abs(case_diff))

    all_differences = list(differences.values())

    # Print some information
    print(f"Number of different patches: {len(all_differences)}")
    print(f"Max diff: {max(all_differences)}")
    print(f"Min diff: {min(all_differences)}")

    threshold = list(reversed(sorted(all_differences)))[number_of_boxes]

    included = [d for d in all_differences if d > threshold]

    # Select a RGB color for the boxes
    color = (255, 0, 0)

    # Loop over the contours
    for index, c in enumerate(cnts):
        # If the total difference exceeds the threshold, draw the box
        if differences[index] > threshold:
            x, y, w, h = positions[index]
            cv2.rectangle(imageA, (x, y), (x + w, y + h), color, 2)
            cv2.rectangle(imageB, (x, y), (x + w, y + h), color, 2)
            cv2.rectangle(diff, (x, y), (x + w, y + h), color, 2)

    # Merge the images vertically and save the result
    merged = np.concatenate((imageA, imageB), axis=0)
    cv2.imwrite(output, merged)

    if save_all_figures:
        # Save the output images
        cv2.imwrite('first_labeled.png', imageA)
        cv2.imwrite('second_labeled.png', imageB)
        cv2.imwrite('difference.png', diff)
        cv2.imwrite('thresh.png', thresh)
