#!/bin/bash
# Bash script to launch the batch mode of met3d using our front end and pipeline.

# Path to the met3d binary
met3d_path=/scratch/o/Oriol.Tinto/tmp/met3d_again/bin//met3d

export QT_DEBUG_PLUGINS=1
echo "Starting batch mode..."
# Define the paths to our frontend and pipeline
workdir=$(pwd)

frontend=${workdir}/temp.frontend.cfg
pipeline=${workdir}/temp.pipeline.cfg

# Check that the frontend and pipeline files exist

if [ ! -f ${frontend} ]; then
    echo "Could not find frontend config file: ${frontend}"
    exit 1
fi

if [ ! -f ${pipeline} ]; then
    echo "Could not find pipeline config file: ${pipeline}"
    exit 1
fi


unset QT_QPA_PLATFORM_PLUGIN_PATH
env > environment.txt

which met3d
# Launch met3D
${met3d_path} --frontend=${frontend} --pipeline=${pipeline} &> ${workdir}/temp.met3d.log

