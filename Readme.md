# Script to create the Compression ratio vs Structural Similarity plots

To create the figure:
- Install the requirements
- Modify the file 'utils4met3d/batchmode.sh' to point to the met3d binary.
- Modify the figure configuration file 'examples/cr_vs_ssim/configuration.yaml' to point to the proper data.

In case of wanting to use different session files, pipelines or frontend, there are few parameters. Place them with the proper names in 'utils4met3d/sessions' and  'utils4met3d/templates'.


In the front-end, we set `loadSessionOnStart=%KIND%` to be that selectable from the configuration file.

In the pipeline, in the NWPPipeline we set `1\path=%PATH_TO_FOLDER%` to allow the script to point to the proper compressed data.

The session files should have the naming `YOUR_NAME.session.config`

That `NAME` should match the one used in the figure configuration file.

Feel free to open an issue or to contact if there's any question.
