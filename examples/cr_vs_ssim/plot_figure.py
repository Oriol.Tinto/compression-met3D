import glob
import json
import logging
import os
import shutil
from pathlib import Path

import xarray
from matplotlib import pyplot as plt

logging.basicConfig(level=logging.WARNING)

import enstools.io
import enstools.compression.api

from enstools.core import cache
# from enstools.encoding.encodings import compression_dictionary_to_string, compression_string_to_dictionary
from enstools.encoding.dataset_encoding import compression_dictionary_to_string
from enstools.encoding.api import DatasetEncoding, VariableEncoding
from enstools.encoding import rules


def compression_string_to_dictionary(compression_string: str) -> dict:
    """
    This function splits a single string containing multiple compression specifications into a dictionary.
    This dictionary will contain a default entry, a coordinates entry and an additional entry for each variable
    explicitly mentioned.

    ----------
    compression_specification: str

    Returns
    -------
    compression_specification: dict
    """

    compression_dictionary = {}

    cases = compression_string.split(rules.VARIABLE_SEPARATOR)
    for case in cases:
        if case.strip():
            if rules.VARIABLE_NAME_SEPARATOR in case:
                variable, spec = case.split(rules.VARIABLE_NAME_SEPARATOR)
            else:
                variable = rules.DATA_DEFAULT_LABEL
                spec = case
            compression_dictionary[variable] = spec

    # Default for data variables if not explicitly specified
    if rules.DATA_DEFAULT_LABEL not in compression_dictionary:
        compression_dictionary[rules.DATA_DEFAULT_LABEL] = rules.DATA_DEFAULT_VALUE

    # Default for coordinates will be set if it was not explicitly specified in the string
    if rules.COORD_LABEL not in compression_dictionary:
        compression_dictionary[rules.COORD_LABEL] = rules.COORD_DEFAULT_VALUE
    return compression_dictionary



class PersistentDictionary(dict):
    """
    A dictionary linked with a file.
    When the dictionary is instantiated it loads the corresponding file if it exists.
    Every time a new element is added the new file is saved.
    """

    def __init__(self, path: Path = None, parent: "PersistentDictionary" = None, **kwargs):
        super().__init__(**kwargs)
        self.path = path
        self.parent = parent
        assert (parent is not None) + (path is not None) == 1, \
            f"Class {type(self)} only accepts a path or a parent, not both"
        if self.path:
            self.load()

    def save(self):
        if self.parent is None:
            with self.path.open("w") as f:
                to_save = {key: self[key] for key in self.keys()}
                json.dump(to_save, f, indent=4)
        else:
            self.parent.save()

    def load(self):
        if self.path.exists():
            with self.path.open("r") as f:
                loaded_data = json.load(f)
            for key, item in loaded_data.items():
                self[key] = item

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            key = str(key)
        super().__setitem__(key, value)
        self.save()

    def __getitem__(self, key):
        if not isinstance(key, str):
            key = str(key)
        return super().__getitem__(key)

    def reload(self):
        self.clear()
        self.load()


# Deactivate Dask cache to prevent bug
cache.unregister()


def main():
    import glob
    from enstools.core.tempdir import TempDir
    from utils4met3d.launch_from_python import get_configuration, setup_functions, structural_similarity, \
        Met3dException, get_compression_ratio

    # Get the configuration
    cfg = get_configuration()

    # Create the function to run the session, the kind of session (kind of plot) will be determined by the configuration
    run_session = setup_functions(cfg)
    ####################################################################################################################
    # Get some parameters from the configuration file
    # Path to the data
    reference_data = cfg.data.source
    # Get list of files to compress
    files = glob.glob(reference_data)

    # Create a temporary directory to store the data that will be used to create the figures
    working_data_directory = TempDir(parentdir="/scratch-local/").getpath()

    plots_path = Path("plots")
    # If it doesn't exist, create a directory to store the intermediate plots
    if not plots_path.exists():
        plots_path.mkdir()

    ####################################################################################################################
    # Create reference figure

    # Copy the files to the working directory to produce the reference
    for file in files:
        shutil.copy(file, working_data_directory)

    reference_file_path = plots_path / f"{cfg.kind}_reference.png"
    if not reference_file_path.exists():
        run_session(working_data_directory, destination=reference_file_path)

    ####################################################################################################################
    # Create figure for the compressed data
    # A different figure will be created fo reach compression mode and parameter specified in the configuration file

    # Temporary folder for store compressed data

    # If the results file exist, load it, otherwise initialize an empty dictionary for the results:

    results = PersistentDictionary(Path(".").resolve() / cfg.results_file)

    colors = {}
    # Loop over compression cases
    for compression_mode in cfg.compression:

        parameters = cfg.compression[compression_mode]

        parameters.sort()

        # If the results for this compression mode don't exist,
        # initialize lists for the compression ratios and the ssims
        if compression_mode not in results:
            results[compression_mode] = {
                "compression_ratios": {},
                "ssims": {},
            }
        for parameter in parameters:
            # If the results for this parameter exist, continue
            if str(parameter) in results[compression_mode]["compression_ratios"]:
                print(f"{compression_mode} - {parameter} already exists")
                continue
            # Delete all files in the temporary folder
            for file in os.listdir(working_data_directory):
                os.remove(os.path.join(working_data_directory, file))

            compression = f"{compression_mode},{parameter}"
            # Define a filename for each compression case:
            compressed_file_path = plots_path / f"{cfg.kind}_{compression}.png"

            if compression_mode == "lossy,zfp,accuracy":
                compression = zfp_relative_fix(files, compression)

            # Fix compression for special variables
            special_variables = ["hyai", "hyam", "hybi", "hybm"]

            
            compression_dict = compression_string_to_dictionary(compression)
            for variable in special_variables:
                compression_dict[variable] = "lossless"

            # compression_for_special_variables = "hyai:lossless hyam:lossless hybi:lossless hybm:lossless"
            compression_fixed = compression_dictionary_to_string(compression_dict)

            # Launch compression using enstools compress
            enstools.compression.api.compress(files, output=working_data_directory,
                                              compression=compression_fixed, emulate=True)

            # Get the compression ratio
            compression_ratio = get_compression_ratio(files, compression)

            print(f"{compression}-> {compression_ratio:.2f}x")

            # Run the case for the compressed data
            try:
                run_session(data_folder=working_data_directory,
                            destination=compressed_file_path)
                ssim = structural_similarity(reference_file_path, compressed_file_path)
                # Save results
                results[compression_mode]["compression_ratios"][parameter] = compression_ratio
                results[compression_mode]["ssims"][parameter] = ssim

                ########################################################################################################
                print(f"Compression: {compression} - SSIM: {ssim:2f}")
            except Met3dException:
                print(f"Compression: {compression} Failed")
                log_name = f"fail_{compression_mode}_{parameter}.log"
                print(f"Saving log file into {log_name!r}")
                shutil.move("temp.met3d.log", log_name)

        results.save()

        # It might happen that the results dictionary contains more values than the ones actually specified in the
        # configuration file. For instance if you keep modifying the cases.
        # I want to keep only the cases that are currently in the configuration file instead of all the values in
        # the dictionary.

        # Clear and reload so all the keys become strings
        results.reload()

        x = [results[compression_mode]["compression_ratios"][str(p)] for p in parameters]
        y = [results[compression_mode]["ssims"][str(p)] for p in parameters]

        # Sort two lists in the same order
        x, y = zip(*sorted(zip(x, y)))

        lines = plt.plot(x,
                         y,
                         "o-",
                         label=compression_mode,
                         alpha=.5,
                         )

    if cfg.legend is True:
        plt.legend()
    # Add the extra points corresponding to Figure 4 or 5.
    extra_points = cfg.get(cfg.kind).points
    for case in extra_points:
        name = list(case.keys())[0]
        pos, letter = case[name]
        x, y = pos

        color = cfg.get(cfg.kind).colors[name]
        plt.scatter(x, y, c=color, linewidths=1.5, edgecolor="k")
        plt.annotate(f"  {letter}", (x, y-.005), alpha=0.5)

    # Remove top and right borders
    plt.gca().spines["top"].set_visible(False)
    plt.gca().spines["right"].set_visible(False)
    # Remove ticks
    plt.gca().tick_params(axis="both", which="both", length=0)

    # plt.xscale("log")
    # plt.yscale("log")
    plt.xlabel("Compression ratio")
    plt.ylabel("SSIM")
    plt.grid(alpha=0.5)

    print(f"Saving figure to {cfg.figure_name!r}")
    plt.xlim(cfg.range.x)
    plt.ylim(cfg.range.y)
    plt.savefig(cfg.figure_name)

    ####################################################################################################################
    # Delete temporary files
    current_script_folder = Path(__file__).resolve().parent
    temporary_files = list(current_script_folder.glob("temp.*"))
    
    for f in temporary_files:
        f.unlink()


def zfp_relative_fix(files: list, compression: str) -> dict:
    from enstools.io import read

    relative_factor = float(compression.split(",")[-1])
    all_variables = {}
    for file in files:
        with read(file) as ds:
            time_slice = ds.isel(time=-1)
            ranges = {var: variable_range(time_slice[var]) for var in time_slice.data_vars}
            compression_dictionary = {var: relative_factor * ran for var, ran in ranges.items()}
            compression_dictionary = {var: f"lossy,zfp,accuracy,{ran}" for var, ran in compression_dictionary.items()}
            all_variables = {**all_variables, **compression_dictionary}
    # return compression_dictionary_to_string(all_variables)
    return DatasetEncoding.get_a_single_compression_string(all_variables)


def variable_range(data_array: xarray.DataArray) -> float:
    return float(data_array.max() - data_array.min())


if __name__ == "__main__":
    main()
